// Copyright (C) 2023 David Thompson <dave@spritely.institute>
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

async function init() {
  const canvas = document.getElementById("canvas");
  const ctx = canvas.getContext("2d");
  const [_gridSize, gridRef, gridSet, gridCycle, gridUpdate] =
        await Scheme.load_main("/wireworld.wasm", {});
  const gridSize = Number(_gridSize), tileSize = 16;
  const canvasSize = gridSize * tileSize;
  const empty = 0, cu = 1, ehead = 2, etail = 3;
  let paused = true, mouseX = 0, mouseY = 0;

  function render() {
    requestAnimationFrame(() => {
      for (var y = 0; y < gridSize; y++) {
        for (var x = 0; x < gridSize; x++) {
          const t = Number(gridRef.call(BigInt(x), BigInt(y)));
          switch(t) {
          case empty:
            ctx.fillStyle = "#fff6d3";
            break;
          case cu:
            ctx.fillStyle = "#f9a875";
            break;
          case ehead:
            ctx.fillStyle = "#7c3f58";
            break;
          case etail:
            ctx.fillStyle = "#eb6b6f";
            break;
          }
          ctx.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
          if(x == mouseX && y == mouseY) {
            ctx.fillStyle = "#00000020";
            ctx.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
          }
        }
      }
    });
  }

  function update() {
    if(!paused) {
      gridUpdate.call();
      render();
    }
    setTimeout(update, 100);
  }

  function pixelToTile(p) {
    return Math.min(Math.max(Math.floor(p / tileSize), 0), gridSize - 1);
  }

  function cycleTile(x, y) {
    gridCycle.call(BigInt(x), BigInt(y));
    render();
  }

  function setTile(x, y, t) {
    gridSet.call(BigInt(x), BigInt(y), BigInt(t));
    render();
  }

  function clearTile(x, y) {
    setTile(x, y, empty);
  }

  function togglePause() {
    paused = !paused;
  }

  canvas.width = canvasSize;
  canvas.height = canvasSize;
  canvas.addEventListener("mousemove", (event) => {
    mouseX = pixelToTile(event.offsetX);
    mouseY = pixelToTile(event.offsetY);
    if(event.buttons == 1) { // left
      setTile(mouseX, mouseY, cu);
    } else if(event.buttons == 2) { //right
      clearTile(mouseX, mouseY);
    }
    render();
  });
  canvas.addEventListener("mousedown", (event) => {
    const x = pixelToTile(event.offsetX);
    const y = pixelToTile(event.offsetY);
    if(event.button == 0) { // left
      cycleTile(x, y);
    } else if(event.button == 2) { // right
      clearTile(x, y);
    }
  });
  canvas.addEventListener("contextmenu", (event) => event.preventDefault());
  document.addEventListener("keydown", (event) => {
    if(event.code == "Space") togglePause();
  });
  update();
  render();
}
window.addEventListener("load", init);
