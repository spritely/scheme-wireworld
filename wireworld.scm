;;; Copyright (C) 2023 David Thompson <dave@spritely.institute>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define grid-size 40)
(define empty 0)
(define cu 1)    ; copper
(define ehead 2) ; electron head
(define etail 3) ; electron tail

(define (make-grid)
  (make-bytevector (* grid-size grid-size) 0))

(define (grid-ref grid x y)
  (bytevector-u8-ref grid (+ (* y grid-size) x)))

(define (grid-ref/wrap grid x y)
  (bytevector-u8-ref grid (+ (* (modulo y grid-size) grid-size)
                             (modulo x grid-size))))

(define (grid-set! grid x y t)
  (bytevector-u8-set! grid (+ (* y grid-size) x) t))

(define (neighbors grid x y)
  (define (check x y)
    (if (= (grid-ref/wrap grid x y) ehead) 1 0))
  (+ (check (- x 1) (- y 1))
     (check x (- y 1))
     (check (+ x 1) (- y 1))
     (check (+ x 1) y)
     (check (+ x 1) (+ y 1))
     (check x (+ y 1))
     (check (- x 1) (+ y 1))
     (check (- x 1) y)))

(define (update from to)
  (do ((y 0 (+ y 1)))
      ((= y grid-size))
    (do ((x 0 (+ x 1)))
        ((= x grid-size))
      (let* ((t (grid-ref from x y))
             (t* (cond
                  ((= t empty) empty)
                  ((= t cu)
                   (if (<= 1 (neighbors from x y) 2) ehead cu))
                  ((= t ehead) etail)
                  ((= t etail) cu))))
        (grid-set! to x y t*)))))

(define (copy from to)
  (let ((k (bytevector-length from)))
    (do ((i 0 (+ i 1)))
        ((= i k))
      (bytevector-u8-set! to i (bytevector-u8-ref from i)))))

(define grid-a (make-grid))
(define grid-b (make-grid))

(define (update-and-swap)
  (update grid-a grid-b)
  (copy grid-b grid-a))

(define (ref x y)
  (grid-ref grid-a x y))

(define (set x y t)
  (grid-set! grid-a x y t))

(define (cycle x y)
  (set x y (modulo (+ (ref x y) 1) 4)))

(values grid-size ref set cycle update-and-swap)
