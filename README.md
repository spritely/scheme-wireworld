# Scheme Wireworld demo

This repository demonstrates a small Scheme program that can be
compiled with [Guile Hoot](https://gitlab.com/spritely/guile-hoot) and
run inside a sufficiently recent web browser.

## How to run the demo

This is a bleeding edge demo that requires a lot of very fresh
software.  GNU Guile built from the main branch, Guile Hoot built from
the main branch, and a bleeding edge browser such as Chrome Canary.

You need to get the browser on your own, but for getting a working
Guile and Hoot we have provided a [Guix](https://guix.gnu.org)
manifest file that can be used to setup the environment.  Once Guix is
installed, just run:

```
guix shell
```

It will fail the first time and tell you how to confirm that you trust
this repo.  Run `guix shell` again after that and you should be good
to go.  Guile in particular takes a long time to build so it will be
awhile.

Once your environment is up, build `wireworld.wasm` like so:
> You can find the commands executed in `Makefile`.

```
make
```

Then launch a simple web server:

```
make serve
```

Once the web server is up, visit http://localhost:8088 and, if your
browser supports WASM GC, you should see Wireworld!

## Interacting with the demo

- Press the left mouse button and move the cursor to draw copper.
- Press the right mouse button and move the cursor to erase.
- Left click on a cell to cycle its type (copper becomes electron
  head, etc.)
- Right click on a cell to clear it.
- Press the space bar to pause or resume the simulation.

Have fun!

## License

The Wireworld Scheme and JS code is licensed under Apache 2.0.
`web-server.scm` is borrowed from
[Haunt](https://dthompson.us/projects/haunt.html) which is under the
GPLv3 license.
